const express = require("express");
const mongoose = require("mongoose");
const ShortUrl = require("./models/shortUrl");
const app = express();
const dotenv = require("dotenv");

//HIDES SENSITIVE STRINGS
dotenv.config();

//CONNECTS TO MONGODB ATLAS
mongoose.connect(process.env.MongoURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

//ENABLES VIEW ENGINE
app.set("view engine", "ejs");

//TO PARSE THE BODY OF HTML
app.use(express.urlencoded({ extended: false }));

app.get("/", async (req, res) => {
  const shortUrls = await ShortUrl.find();
  res.render("index", { shortUrls: shortUrls });
});

//ONSUBMIT IT CREATES A NEW SHORT URL AND STORES INTO THE DB
app.post("/shortUrls", async (req, res) => {
  await ShortUrl.create({ full: req.body.fullUrl });

  res.redirect("/");
});

app.get("/:shortUrl", async (req, res) => {
  //FINDS THE PARAM URL AND INCREMENTS THE CLICK
  const shortUrl = await ShortUrl.findOne({ short: req.params.shortUrl });
  if (shortUrl == null) return res.sendStatus(404);

  shortUrl.clicks++;
  shortUrl.save();
  //REDIRECTS TO THE ORIGINAL FULL URL
  res.redirect(shortUrl.full);
});

app.listen(process.env.PORT || 5000);
