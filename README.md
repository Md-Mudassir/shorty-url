# Shorty-URL

URL shortener built using Node.js, Express, and MongoDB.

Explore: https://sh0rty.herokuapp.com/
